# Setup for Unryddle Survey

## If you have not cloned the repo, follow the commands below to get started

1. Clone the repository - `$ git clone https://jlevi@bitbucket.org/jlevi/unryddle-survey.git`

2. Cd in the repo directory: - `$ cd path/to/unryddle-survey`

3. run npm install to install dependencies - `$ npm install`

4. run gulp to start a local server and watch for changes - `$ gulp`


## if you already have the clone, but do not have npm dependencies installed, do the following

1. run git pull to get the latest repo updates(enter the correct password if requested): `$ git pull`

2. once you have the updates run npm install to install dependencies: `$ npm install`

3. run gulp to start a local server and watch for changes - `$ gulp`


## Updating repo
Once you have made any local changes to your repo, commit the changes and run
`$ git push`
type in your password if requested