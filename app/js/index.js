(function($){
    'use strict';

    // define survey endpoint where results will be sent
    var surveyPublicRef = firebase.database().ref('public_survey');

    // define options for success, error modals
    var modalOptions = {
        backdrop: true,
        keyboard: false,
        focus: true,
        show: true
    };

    // define validation rules for survey form
    $('#survey-form').validate({
        rules:{
            'chosenCareer':{ required: true },
            'careerChoiceOwn': { required: true },
            'careerChoiceParents': { required: true },
            'careerChoiceTeacher': { required: true },
            'choiceDiffParents': { required: true },
            'choiceDiffTeachers': { required: true },
            'adequateCareerInfo': { required: true },
            'careerConsultPros': { required: true },
            'indProChoice': { required: true },
            'careerInterest': { required: true },
            'careerChoiceSalary': { required: true },
            'careerPopularity': { required: true },
            'careerSecurity': { required: true },
            'careerMentor': { required: true },
            'needCareerGuidance': { required: true },
            'surveyAge': { required: true },
            'education_level': { required: true }
        }
    });


    // form submission event handler
    $('#complete-survey').off('click').on('click', function(e) {
        e.preventDefault();

        // check if form is valid
        if($('#survey-form').valid()){

            // set object payload for survey endpoint store
            var payload = {
                surveyTakerAge: $('#surveyAge option:selected').val(),
                surveyTakerEducation: $('#education_level option:selected').val(),
                isCareerChosen: getRadioVals($("input[name='chosenCareer']")),
                wasCareerChoiceOwn: getRadioVals($("input[name='careerChoiceOwn']")),
                seekParentsAdvice: getRadioVals($("input[name='careerChoiceParents']")),
                seekTeachersAdvice: getRadioVals($("input[name='careerChoiceTeacher']")),
                isChoiceDifferentParents: getRadioVals($("input[name='choiceDiffParents']")),
                isChoiceDifferentTeachers: getRadioVals($("input[name='choiceDiffTeachers']")),
                adequateInfoAvailable: getRadioVals($("input[name='adequateCareerInfo']")),
                consultIndustryPros: getRadioVals($("input[name='careerConsultPros']")),
                adviceFromPros: getRadioVals($("input[name='indProChoice']")),
                interestInCareerChoice: getRadioVals($("input[name='careerInterest']")),
                careerChoiceBySalary: getRadioVals($("input[name='careerChoiceSalary']")),
                careerChoiceByPopularity: getRadioVals($("input[name='careerPopularity']")),
                careerChoiceBySecurity: getRadioVals($("input[name='careerSecurity']")),
                careerChoiceByMentor: getRadioVals($("input[name='careerMentor']")),
                careerChoiceByGuidance: getRadioVals($("input[name='needCareerGuidance']"))
            }
            // set unique key for survey endpoint
            var surveyPublicKey = surveyPublicRef.push().key;

            // write payload to survey endpoint with unique key set above
            firebase.database().ref("public_survey/" + surveyPublicKey).set(payload, function(error){
                if(error) {
                    console.log(error);
                    $('#errorModal').modal(modalOptions);
                } else {
                    $('#successModal').modal(modalOptions);
                }
            });
        } else {
            $('#invalid-form').removeClass('hide');
        }
    });

    // clear input fields when modals are hidden
    $('#successModal').on('hidden.bs.modal', function(e) {
        $('#surveyAge option:selected').val('');
        $('#education_level option:selected').val('');
        $('input[type=radio]').attr('checked', false);
    });
    $('#errorModal').on('hidden.bs.modal', function(e) {
        $('#surveyAge option:selected').val('');
        $('#education_level option:selected').val('');
        $('input[type=radio]').attr('checked', false);
    });

    // function to get selected radio value from radio input object
    function getRadioVals(obj){
        var radioVal = null;
        obj.each(function() {
            var $this = $(this);
            if($this.prop('checked')) {
                radioVal = $this.val();
            }
        });

        return radioVal;
    }

}(jQuery));